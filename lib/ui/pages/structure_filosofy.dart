import 'package:flutter/material.dart';

class StructureFilosofy extends StatelessWidget {
  const StructureFilosofy({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: Row(
            children: [
              Expanded(
                  child: Container(
                      color: Colors.red,
                      child: const Center(
                          child: Text(
                        'En',
                        style: TextStyle(
                            fontSize: 80, fontWeight: FontWeight.bold),
                      )))),
              Expanded(
                  child: Container(
                      color: Colors.white.withOpacity(0.9),
                      child: Image.asset('assets/flutter.png'))),
              Expanded(
                  child: Container(
                      color: Colors.blue,
                      child: const Center(
                          child: Text(
                        'todo',
                        style: TextStyle(fontSize: 80, fontWeight: FontWeight.bold),
                      )))),
            ],
          ),
        ),
        Expanded(
          child: Row(
            children: [
              Expanded(
                  child: Container(
                      color: Colors.green,
                      child: const Center(
                          child: Text(
                        'es',
                        style: TextStyle(fontSize: 80, fontWeight: FontWeight.bold),
                      )))),
              Expanded(
                  child: Container(
                      color: Colors.deepOrangeAccent,
                      child: const Center(
                          child: Text(
                        'un',
                        style: TextStyle(fontSize: 80, fontWeight: FontWeight.bold),
                      )))),
            ],
          ),
        ),
        Expanded(
            child: Container(
                color: Colors.deepPurple,
                child: const Center(
                    child: Text(
                  'widget',
                  style: TextStyle(fontSize: 80, fontWeight: FontWeight.bold),
                )))),
      ],
    );
  }
}
