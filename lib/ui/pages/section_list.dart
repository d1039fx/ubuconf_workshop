import 'package:flutter/material.dart';
import 'package:init_flutter/ui/pages/structure_filosofy.dart';
import 'package:init_flutter/ui/pages/useit/platform_list_page.dart';
import 'package:url_launcher/url_launcher.dart';

mixin SectionList {
  List<Widget> sectionList = [
    Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Column(
        children: [
          Expanded(
            child: Row(
              children: [
                Expanded(
                    child: Center(
                  child: Image.asset(
                    'assets/ubuntu-logo.png',
                  ),
                )),
                Expanded(
                    child: Image.asset(
                  'assets/flutter.png',
                )),
              ],
            ),
          ),
          const Expanded(
              child: Center(
            child: Text(
              'Desarrollo de Software con Flutter en ubuntu',
              style: TextStyle(fontSize: 50),
            ),
          ))
        ],
      ),
    ),
    Column(
      children: [
        Expanded(
          child: Center(child: Image.asset('assets/anillo.png')),
        ),
        const Expanded(
            child: Center(
          child: Text(
            'El señor de los anillos.',
            style: TextStyle(fontSize: 50),
          ),
        ))
      ],
    ),
    Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(child: Image.asset('assets/flutter.png')),
              const Expanded(
                  child: Icon(
                Icons.favorite,
                color: Colors.red,
                size: 200,
              )),
              Expanded(
                  child: Image.network(
                      'https://uxwing.com/wp-content/themes/uxwing/download/brands-and-social-media/dart-programming-language-icon.png')),
            ],
          ),
        ],
      ),
    ),
    const Column(
      children: [
        Expanded(
            child: Icon(
          Icons.thumb_down_outlined,
          color: Colors.red,
          size: 100,
        )),
        Expanded(
          child: Padding(
            padding: EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Icon(
                      Icons.circle,
                      size: 15,
                      color: Colors.red,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Text(
                        'Desconocimiento del lenguaje Dart',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Icon(
                      Icons.circle,
                      size: 15,
                      color: Colors.red,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Text(
                        'Relativamente nuevo aunque con una comunidad creciente',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Icon(
                      Icons.circle,
                      size: 15,
                      color: Colors.red,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Text(
                        'Depende en parte de los SDK de las plataformas para desarrollar funcionalidades propias de las mismas',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Icon(
                      Icons.circle,
                      size: 15,
                      color: Colors.red,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Text(
                        'Para los desarrolladores que trabajan nativamente se les dificulta ya que el código está implícito con el diseño de la interfaz',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        )
      ],
    ),
    const Column(
      children: [
        Expanded(
            child: Icon(
          Icons.thumb_up_outlined,
          color: Colors.green,
          size: 100,
        )),
        Expanded(
          child: Padding(
            padding: EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Icon(
                      Icons.circle,
                      size: 15,
                      color: Colors.green,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Text(
                        'Hot Reload',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Icon(
                      Icons.circle,
                      size: 15,
                      color: Colors.green,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Text(
                        'Un código único para dominarlos a todos',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Icon(
                      Icons.circle,
                      size: 15,
                      color: Colors.green,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Text(
                        'Actualmente se puede desarrollar en 7 plataformas',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Icon(
                      Icons.circle,
                      size: 15,
                      color: Colors.green,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Text(
                        'Una lista creciente de plugins',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Icon(
                      Icons.circle,
                      size: 15,
                      color: Colors.green,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Text(
                        'Reactivo',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Icon(
                      Icons.circle,
                      size: 15,
                      color: Colors.green,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Text(
                        'Calidad Nativa',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Icon(
                      Icons.circle,
                      size: 15,
                      color: Colors.green,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Text(
                        'Los Widgets se pueden tratar como objetos y son fácilmente personalizables',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Icon(
                      Icons.circle,
                      size: 15,
                      color: Colors.green,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Expanded(
                      child: Text(
                        'Dart es un lenguaje con una curva de aprendizaje baja ya que su sintaxis es muy familiar con otros lenguajes.',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        )
      ],
    ),
    Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        children: [
          Expanded(
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: Image.asset('assets/lousan.png'))),
          Expanded(
            child: Column(
              children: [
                const Expanded(child: SizedBox()),
                TextButton(
                    onPressed: () async {
                      if (!await launchUrl(
                          Uri.parse('https://flutter.dev/showcase'))) {
                        throw Exception(
                            'Could not launch https://flutter.dev/showcase');
                      }
                    },
                    child: const Text(
                      'Entre otros...',
                      style: TextStyle(fontSize: 30),
                    )),
                const Expanded(child: SizedBox()),
              ],
            ),
          )
        ],
      ),
    ),
    Column(
      children: [
        Expanded(child: PlatformListPage()),
      ],
    ),
    const StructureFilosofy(),
    Column(
      children: [
        Expanded(
          flex: 2,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: Image.asset('assets/widget-tree.png')),
                ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: Image.asset('assets/tree.png')),
              ],
            ),
          ),
        ),
        const Expanded(
            child: Center(
          child: Text(
            'Arbol de widgets',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 40),
          ),
        ))
      ],
    ),
    Column(
      children: [
        Expanded(
          flex: 2,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: Image.asset('assets/widget-structure.png')),
                ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: Image.asset('assets/screen-structure.png')),
              ],
            ),
          ),
        ),
        const Expanded(
            child: Center(
          child: Text(
            'Ejemplo de structura',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 40),
          ),
        ))
      ],
    ),
    const Column(
      children: [
        Expanded(
          flex: 2,
          child: Row(
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.grid_view,
                      size: 100,
                    ),
                    Text(
                      'StatelessWidget',
                      style: TextStyle(fontSize: 35),
                    ),
                    Text(
                      'Inmutable',
                      style: TextStyle(fontSize: 15),
                    )
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.interests_outlined,
                      size: 100,
                    ),
                    Text(
                      'Statefulwidget',
                      style: TextStyle(fontSize: 35),
                    ),
                    Text(
                      'Mutable',
                      style: TextStyle(fontSize: 15),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        Expanded(
            child: Center(
                child: Text(
          'Flutter => Reactivo',
          style: TextStyle(fontSize: 50, fontWeight: FontWeight.bold),
        )))
      ],
    ),
  ];
}
