mixin PlatformList{
  final List<String> platformImageList = [
    'assets/ubuntu-logo.png',
    'assets/android-logo.png',
    'assets/ios-logo.png',
    'assets/macos-logo.png',
    'assets/windows-logo.png',
    'assets/web-logo.png',
    'assets/raspberrypi-logo.png',
    'assets/fuchsia-logo.png',
  ];
}