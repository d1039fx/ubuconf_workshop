import 'package:flutter/material.dart';
import 'package:init_flutter/ui/pages/useit/platform_list.dart';

class PlatformListPage extends StatelessWidget with PlatformList {
  PlatformListPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GridView(
        gridDelegate:
            const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4),
        children: platformImageList
            .map<Container>((platformImage) => Container(
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(width: 5),
            borderRadius: BorderRadius.circular(20)
          ),
              child: Image.asset(
                    platformImage,
          
                  ),
            ))
            .toList());
    // return Wrap(
    //   children: platformImageList
    //       .map<Image>((platformImage) => Image.asset(platformImage, height: 100,))
    //       .toList(),
    // );
  }
}
