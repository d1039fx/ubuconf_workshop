import 'package:flutter/material.dart';
import 'package:init_flutter/ui/menu.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:init_flutter/ui/pages/section_list.dart';

class PrincipalPage extends StatefulWidget {
  const PrincipalPage({super.key});

  @override
  State<PrincipalPage> createState() => _PrincipalPageState();
}

class _PrincipalPageState extends State<PrincipalPage> with Menu, SectionList {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Flutter Ubuntu App'),
      ),
      body: Row(
        children: [
          Container(
            color: const Color(0xFF333333),
            width: 220,
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              children: menuList
                  .map<FilledButton>((menu) => FilledButton(
                      style: FilledButton.styleFrom(
                        textStyle: GoogleFonts.ubuntu(),
                          backgroundColor: Colors.transparent),
                      onPressed: () {
                        int indexCurrent = menuList.indexWhere((element) => element['label'] == menu['label']);
                        setState(() {
                          selectedIndex = indexCurrent;
                        });
                      },
                      child: SizedBox(
                        height: 30,
                        child: Row(
                          children: [
                            Icon(menu['item']),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(menu['label'])
                          ],
                        ),
                      )))
                  .toList(),
            ),
          ),
          Expanded(
            child: Builder(builder: (context) {
              return Center(
                child: Center(
                  child: switch (selectedIndex) {
                    1 => sectionList[selectedIndex],
                    2 => sectionList[selectedIndex],
                    3 => sectionList[selectedIndex],
                    4 => sectionList[selectedIndex],
                    5 => sectionList[selectedIndex],
                    6 => sectionList[selectedIndex],
                    7 => sectionList[selectedIndex],
                    8 => sectionList[selectedIndex],
                    9 => sectionList[selectedIndex],
                    10 => sectionList[selectedIndex],
                    _ => sectionList[0]
                  },
                ),
              );
            }),
          )
        ],
      ),
    );
  }
}
