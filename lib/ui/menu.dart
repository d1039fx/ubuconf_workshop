import 'package:flutter/material.dart';

mixin Menu{
  List<Map<String, dynamic>> menuList = [
  {'item': Icons.home_outlined, 'label': 'Inicial'},
  {'item': Icons.description_outlined, 'label': 'Descripcion'},
  {'item': Icons.favorite_outline, 'label': 'Flutter <3 Dart'},
  {'item': Icons.thumb_down_outlined, 'label': 'Desventajas'},
  {'item': Icons.thumb_up_outlined, 'label': 'Ventajas'},
  {'item': Icons.apartment_outlined, 'label': 'Lo usan'},
  {'item': Icons.devices_outlined, 'label': 'Plataformas'},
  {'item': Icons.menu_book_outlined, 'label': 'Filosofia'},
  {'item': Icons.account_tree_outlined, 'label': 'Arbol de widgets'},
  {'item': Icons.construction_outlined, 'label': 'Estructura basica'},
  {'item': Icons.science_outlined, 'label': 'Reactivo'},

  ];
}