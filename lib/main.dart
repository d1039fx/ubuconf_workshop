import 'package:flutter/material.dart';
import 'package:init_flutter/ui/pages/principal_page.dart';
import 'package:yaru/yaru.dart' as yaru;

void main() {
  runApp(const UbuntuApp());
}

class UbuntuApp extends StatelessWidget {
  const UbuntuApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Ubuntu App',
      theme: yaru.yaruDark,
      home: const PrincipalPage(),
    );
  }
}
